import analytics from '@react-native-firebase/analytics';

export const trackScreen = async (screenName: string): Promise<void> => {
  try {
    await analytics().logScreenView({screen_name: screenName, screen_class: screenName});
  } catch (err) {
    // ignore error
  }
};
