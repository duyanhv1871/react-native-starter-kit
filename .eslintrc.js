module.exports = {
  root: true,
  extends: ['@react-native-community', 'airbnb', 'airbnb/hooks', 'prettier'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'no-null'],
  rules: {
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],
    'constructor-super': 'off', // ts(2335) & ts(2377)
    'getter-return': 'off', // ts(2378)
    'no-const-assign': 'off', // ts(2588)
    'no-dupe-args': 'off', // ts(2300)
    'no-dupe-class-members': 'off', // ts(2393) & ts(2300)
    'no-dupe-keys': 'off', // ts(1117)
    'no-func-assign': 'off', // ts(2539)
    'no-import-assign': 'off', // ts(2539) & ts(2540)
    'no-new-symbol': 'off', // ts(2588)
    'no-obj-calls': 'off', // ts(2349)
    'no-redeclare': 'off', // ts(2451)
    'no-setter-return': 'off', // ts(2408)
    'no-this-before-super': 'off', // ts(2376)
    'no-undef': 'off', // ts(2304)
    'no-unreachable': 'off', // ts(7027)
    'no-unsafe-negation': 'off', // ts(2365) & ts(2360) & ts(2358)
    'no-var': 'error', // ts transpiles let/const to var, so no need for vars any more
    'prefer-const': 'error', // ts provides better types with const
    'prefer-rest-params': 'error', // ts provides better types with rest args over arguments
    'prefer-spread': 'error', // ts transpiles spread to apply, so no need for manual apply
    'valid-typeof': 'off', // ts(2367)
    'no-null/no-null': ['error'],
    'react/jsx-filename-extension': ['error', {extensions: ['.tsx']}],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        ts: 'never',
        tsx: 'never',
      },
    ],
    'import/prefer-default-export': 'off',
    '@typescript-eslint/explicit-function-return-type': [
      'error',
      {
        allowExpressions: true,
        allowTypedFunctionExpressions: true,
      },
    ], // force to define function return type
    'class-methods-use-this': [
      'error',
      {
        exceptMethods: ['componentDidCatch', 'componentDidAppear', 'componentDidDisappear'],
      },
    ],
    'import/no-unresolved': [
      'error',
      {
        ignore: ['@test-utils', '@app', '@core', '@assets', '@auth', '@settings', '@samples'],
      },
    ], // ignore module import
    'max-len': ['error', 120], // change max length for a line to 120
    'no-console': 'error', // don't allow console
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: ['draft', 'draftState'],
      },
    ], // no params reassigned except using immer
    'no-unused-expressions': ['error', {allowShortCircuit: true}], // don't use unused expressions except short circuit
    'no-unused-vars': ['error', {argsIgnorePattern: '^_'}], // don't use unused var except with _ prefix
    '@typescript-eslint/no-explicit-any': ['error'], // forbid to use 'any' type
    'react/jsx-closing-bracket-location': 'off', // let prettier formats the code
    'react/require-default-props': 'off',
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },
  env: {
    jest: true,
  },
};
